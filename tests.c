/*******************************************************************************
*                               TESTS.C
*
* Implementation de toutes les fonctions de test qui pour chacune d'elles teste
* pour une fonction du programme en particulier, l'ensemble des cas inherents
* a cette fonction.
*
*******************************************************************************/

#include "tests.h"


void test_liberation()
{
  majeur_t * table = NULL;
  
  puts("#########\n### test_liberation ###\n#########\n\n");
  puts("\n\n--- Cas liste vide ---\n");
  libererTable(&table);
  puts("");

  puts("--- Cas liste remplie ---\n");
  table = importerListe("anglais.txt");
  printf("// On affiche la taille moyenne des sous-listes pour prouver que la table est bien remplie:\n\n");
  printf("Taille moyenne : %f elements\n", longueurMoyenneSousTables(table));
  libererTable(&table);
  puts("#########\n### test_liberation   [FIN]  ###\n#########\n\n");
}


void test_recherche()
{
  majeur_t * table = NULL;

  puts("#########\n### test_recherche ###\n#########\n\n");

  table = importerListe("anglais.txt");

  puts("\n\n--- Cas mot inconnu ---\n");
  puts("");
  printf("Mot recherche: [%s]\nAdresse de la cellule dans le dictionnaire: [%p]\n", "anticonsitutionellement", (void *) rechercherMot(table, "anticonsitutionellement"));
  puts("L'adresse est 'NULL' car le mot n'est pas dans le dictionnaire comme prevu\n");

  puts("--- Cas mot present ---\n");
  puts("");
  printf("Mot recherche: [%s]\nAdresse de la cellule dans le dictionnaire: [%p]\n", "job", (void *) rechercherMot(table, "job"));
  puts("L'adresse n'est pas 'NULL' donc le mot est present\n");
  printf("Mot: [%s] = [%s]\n\n\n", rechercherMot(table, "job")->mot, rechercherMot(table, "job")->traduction);

  puts("--- Cas dernier mot saisi ---\n");
  puts("");
  puts("On insert le mot 'window' et sa traduction 'fenetre'\n");
  insererCellule(table, "window", "fenetre");
  printf("Mot recherche: [%s]\nAdresse de la cellule dans le dictionnaire: [%p]\n", "window", (void *) rechercherMot(table, "window"));
  puts("L'adresse n'est pas 'NULL' donc le mot est present\n");
  printf("Mot: [%s] = [%s]\n", rechercherMot(table, "window")->mot, rechercherMot(table, "window")->traduction);

  libererTable(&table);
  puts("#########\n### test_recherche   [FIN]  ###\n#########\n\n");
}



void test_insertion()
{
  majeur_t * table = NULL;
  
  puts("#########\n### test_insertion ###\n#########\n\n");

  table = importerListe("anglais.txt");
  
  puts("\n\n--- Cas mot absent ---\n");
  printf("Mot recherche: [%s]\nestMotPresent? = %d\n", "spoon", !(NULL == rechercherMot(table, "spoon")));
  puts("estMotPresent? a 0 nous indique que le mot n'est pas dans le dictionnaire\n\n");
  puts("On insert le mot 'spoon' et sa traduction 'cuillere'\n");
  
  insererCellule(table, "spoon", "cuillere");

  puts("On relance la recherche:");
  printf("Mot recherche: [%s]\nestMotPresent? = %d\n", "spoon", !(NULL == rechercherMot(table, "spoon")));
  puts("Cette fois le mot est present\n");
  printf("Mot: [%s] = [%s]\n\n\n", rechercherMot(table, "spoon")->mot, rechercherMot(table, "spoon")->traduction);


  puts("--- Cas mot deja present ---\n\n");
  puts("On veut inserer le mot [hello]");
  printf("Mot recherche: [%s]\nestMotPresent? = %d\n", "hello", !(NULL == rechercherMot(table, "hello")));
  puts("Le mot est deja present");
  puts("On tente d'inserer le mot [hello]");

  insererCellule(table, "hello", "bonjour");
  puts("L'insertion est donc annulee comme cela etait prevu\n\n\n");


  libererTable(&table);
  puts("#########\n### test_insertion   [FIN]  ###\n#########\n\n");

}



void test_traduction()
{
  majeur_t *        table = NULL;
  char       retour[1000] = {0};	 /* Recoit la traduction d'une phrase complete */
  char           phrase[] = "hello bye sorry i am cheese end";

  puts("#########\n### test_traduction ###\n#########\n\n");

  table = importerListe("anglais.txt");

  puts("\n\n--- Cas mot absent ---\n");
  puts("On essaie de traduire le mot [chair]\n");
  printf("Mot recherche: [%s]\nestMotPresent? = %d\n", "chair", !(NULL == rechercherMot(table, "chair")));
  printf("Traduction: %s = %s\n", "chair", traductionMot("chair", table));
  puts("Le comportement est celui attendu. En cas d'absence de traduction, le mot est retourne tel quel\n\n\n");

  puts("--- Cas mot present ---\n");
  puts("On essaie de traduire le mot [cat]\n");
  printf("Mot recherche: [%s]\nestMotPresent? = %d\n", "cat", !(NULL == rechercherMot(table, "cat")));
  printf("Traduction: ");
  traductionMot("cat", table);

  puts("\n\n\n--- Cas traduction phrase complete ---\n");
  printf("On essaie de traduire la phrase suivante:\n[%s]\n", phrase);
  puts("Seul le mot [chesse] ne possede pas de traduction\n");
  traductionPhrase(phrase, table, retour);
  printf("Traduction:\n[%s]\n", retour);
  puts("Le resultat correspond au contenu du dictionnaire\n\n\n");


  libererTable(&table);
  puts("#########\n### test_traduction   [FIN]  ###\n#########\n\n");

}

void test_longueurMoyenneSousChaines()
{
  majeur_t * table = NULL;
  
  puts("#########\n### test_longueurMoyenneSousChaines ###\n#########\n\n");

  table = importerListe("anglais.txt");
  
	printf("Longueur moyenne: %f\n\n\n", longueurMoyenneSousTables(table));

  puts("#########\n### test_longueurMoyenneSousChaines   [FIN]  ###\n#########\n\n");
  libererTable(&table);
}

/*
*/





/*******************************************************************************
* Tests de toutes les fonctions:
*   - De creation de dictionnaire
*   - D'affichage du dictionnaire
*   - De recherche et d'affichage de motifs
*   - De Liberation du dictionnaire
*
* @entree nomFichier :  Chemin relatif vers le fichier contenant les mots
* @local           r :  Adresse de la racine du dictionnaire
*******************************************************************************/

/*
void test_importerArbre(char * nomFichier)
{

  arbre_t ** r =  importerArbre(nomFichier);

  puts("\n");
  printf("Liste des mots de l'arbre: \n" );
  afficherDico(*r,""); 
  puts("\n");



  printf("--- Recherches de motifs --- \n\n" );

  printf("motif 'qu'\n" );
  motMotif(r, "qu");


  printf("motif 'qui'\n" );
  motMotif(r, "qui");


  printf("motif 'quiche'\n" );
  motMotif(r, "quiche");


  printf("motif 'quit'\n" );
  motMotif(r, "quit");
  puts("\n");


  printf("--- Tests de liberation --- \n\n" );
  printf("Liste des mots de l'arbre (encore rempli) \n" );
  afficherDico(*r,"");

  printf("Test Liberation de l'arbre (deja vide):\n" );
  libererArbre(r);

  puts("");
  printf("Test affichage d'un arbre vide:\n" );
  afficherDico(*r,"");
  puts("");

  printf("Liberation de l'arbre vide:\n" );
  libererArbre(r);

  free(r);
}
*/