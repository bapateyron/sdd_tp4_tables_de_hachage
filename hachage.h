/***************************************************************************************
*                                 HACHAGE.H
*
* Fichier en-tete qui declare les prototypes de toutes les fonctions gerant le hachage
* et la table associee
*
****************************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define HASH_MAX    29
#define TAILLE_MAX  35  /* Taille max pour les mots a traduire */

/*** Structures ************************************************************************/

/* Struture cellule qui composent les sous-chaines de la table majeure */
typedef struct cellule_t
{
  char             *  mot;            /* Mot dans la langue d'origine */
  char             *  traduction;     /* Traduction du mot dans la langue cible */
  struct cellule_t *  suivant;        /* Adresse de la cellule suivante dans la sous-chaine */
} cellule_t;

/* Struture table majeure */
typedef struct majeur
{
  cellule_t *  chaine[HASH_MAX];      /* Liste des sous-chaines de la table */
  int          NBelement[HASH_MAX];   /* Liste contenant la taille de chaque sous-chaine */
} majeur_t;


/*** Prototypes ************************************************************************/

majeur_t      * initialisationTabMaj();
cellule_t     * rechercherMot(majeur_t * table, char * mot);
int             insererCellule(majeur_t * table, char mot[100], char traduction[100]);
majeur_t      * importerListe(char * nomFichier);
unsigned int    hash_string(const char *str);
char          * traductionMot(char * mot, majeur_t * table);
void            traductionPhrase(char * phrase, majeur_t * table, char * retour);
void            libererTable(majeur_t ** table);
float           longueurMoyenneSousTables(majeur_t * table); /*** Bonus ***/ 
