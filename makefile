FLAGS=-Wall -Wextra -ansi -pedantic -g

all: prog.bin

prog.o: main.c hachage.c tests.c
	gcc $(FLAGS) main.c hachage.c tests.c -c

prog.bin: prog.o
	gcc *.o -o prog.exe

clean:
	rm *.o *~ vgcore* *.exe
