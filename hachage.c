/*******************************************************************************
 *                                   HACHAGE.C
 *
 * Implementation des fonctions du fichier hachage.h qui permettent de manipuler
 * le dictionnaire a la base de ce TP.
 *
 ******************************************************************************/


#include "hachage.h"


/*******************************************************************************
 * Retourne un numero suivant un mot qui represente l'emplacement du mot
 * dans la table majeur
 *
 * @entree  	str:  Une chaine de caractere
 * 
 * @return  	int:  Le hash obtenu a partir de la chaine 'str'
 ******************************************************************************/
unsigned int hash_string(const char *str)
{
  unsigned int hash = 5381;                /* Fonction de hachage de D.J. Bernstein */
  const char *s;
  for (s = str; *s; s++)
    hash = ((hash << 5) + hash) + tolower(*s);
  return (hash & 0x7FFFFFFF) % HASH_MAX;
}




/*******************************************************************************
 * Alloue d'abord la table majeure. Puis alloue dans cette table, une liste de
 * sous-chaines de taille HASH_MAX ainsi qu'une liste de meme taille contenant le
 * nombre d'elements presents dans chaque sous-chaine. Ces valeurs sont 
 * respectivement initialisees a NULL et a 0.
 *
 * @local       i:  Indice de parcours
 * 
 * @return  table:  Le pointeur sur la table majeur cree
 ******************************************************************************/
majeur_t * initialisationTabMaj()
{
  majeur_t * table  = (majeur_t *) malloc(sizeof(majeur_t));
  int            i  = 0;

  if(table == NULL)
  {
		fprintf(stderr, "initialisationTabMaj(): Echec allocation\n");
    exit(-1);
	}
  else
  {
    /* On met les valeurs de la table a NULL */
    for(i = 0; i < HASH_MAX; i++)
    {
      table->chaine[i]    = NULL;
      table->NBelement[i] = 0;
    }
  }

  return table;
}


/*******************************************************************************
 * Donne l'adresse dans la table de la cellule correspondant au 'mot' mis en 
 * argument s'il est present.
 *
 * @entree 		 table:  Adresse de la table majeur
 * @entree    	 mot:  Le mot que l'on recherche
 * 
 * @return  parcours:  Adresse de la cellule dans la table contenant le mot
 *                     Ou NULL si le mot n'existe pas dans la table
 ******************************************************************************/
cellule_t * rechercherMot(majeur_t * table, char * mot)
{
  cellule_t	*   parcours  = NULL; /* NULL est le retour par defaut */

  /* On initialise le pointeur de parcours avec le hash du mot qu'on cherche */
  parcours  = table->chaine[hash_string(mot)];

  /* On progresse sur la liste chainee jusqu'a arriver au mot qu'on cherche */
  while (parcours && (strcmp(parcours->mot, mot) != 0) )
  {
    parcours  = parcours->suivant;
  }

  return parcours;
}





/*******************************************************************************
 * Permet d'ajouter une cellule de sous-chaine dans la table majeure avec
 * un mot et une traduction. (L'insertion se fait en debut de sous-chaine)
 *
 * @entree 		 	 table:  Adresse de la table majeur
 * @entree    		 mot:  String contenant le mot de la lange d'origine
 * @entree 	traduction:  Strig contenant la traduction du mot
 * 
 * @local   	 nouveau:  Pointeur de la nouvelle cellule                             
 * @local   	 	 index:  Indice dans la table de la sous-chaine cible
 * 
 * @return  	    code:  Code d'erreur. 1 = Succes , 0 = Echec
 ******************************************************************************/
int insererCellule(majeur_t * table, char * mot, char * traduction)
{
	cellule_t	*	nouveau   = NULL;
  int           index   = 0;
  int            code   = 1; /* 1 = Succes ,  0 = Echec */


  if(rechercherMot(table, mot)) /* Si le mot est deja dans le dictionnaire */
  {
    printf("insererCellule(): Le mot \"%s\" est deja present\n", mot);
  }
  else                          /* Sinon on realise la creation et l'insertion */
  {
    nouveau = (cellule_t *) malloc (sizeof(cellule_t));

    if(nouveau == NULL)
    {
      fprintf(stderr, "insererCellule(): Echec allocation\n");
      code  = 0;
    }
    else
    { 
      nouveau->mot        = (char *) malloc(sizeof(char) * strlen(mot) + 1);        /* +1 pour le '\0' */
      nouveau->traduction = (char *) malloc(sizeof(char) * strlen(traduction) + 1);

      if(nouveau->mot == NULL || nouveau->traduction == NULL) 
      {                                                   /* Si les allocations ont echouees */
        fprintf(stderr, "insererCellule(): Echec allocation\n");
        code  = 0;
      }
      else                                                /* Si les allocations ont reussies */
      {                                                   /* (On aurait pu utiliser une insertion de liste chainee) */
        strcpy(nouveau->mot, mot);                        /* On place dans 'nouveau' le mot et la traduction lue */
        strcpy(nouveau->traduction, traduction);

        index = hash_string(mot);  												/* Calcul du hash pour determiner l'index */

        nouveau->suivant        = table->chaine[index];   /* Chainage de la nouvelle cellule */
        table->chaine[index]    = nouveau;
        table->NBelement[index] ++;												/* On incremente le nombre d'elements de la sous-chaine */
      }
    }
  }
  

	return code;
}




/*******************************************************************************
 * Prend un nom de fichier en argument et cree la structure de dictionnaire
 * avec les lignes de ce fichier. On recherche si le fichier n'existe pas deja
 * avant d'inserer pour eviter les doublons. On retourne l'adresse de la
 * table majeure ainsi cree.
 *
 * @param 	nomFichier : Nom du fichier a importer
 * 
 * @local          mot : String tampon contenant le mot a traduire
 * @local   traduction : String tampon contenant la traduction du mot
 * @local         	 f : Pointeur du fichier
 * @local            n : Recupere le nombre de caracteres lus par fscanf
 * @local         copy : String qui recoit la ligne lue   (mot;mot_traduit)
 * 
 * @return        table: Pointeur sur la table majeur cree
 ******************************************************************************/
majeur_t  *  importerListe(char * nomFichier)
{
  majeur_t *      table   = initialisationTabMaj();
  char     *        mot   = {0};									
  char     * traduction   = {0};									
  FILE     *          f   = fopen(nomFichier, "r");
  int                 n   = 0;    								
  char        copy[100]   = {0};									


  if(f == NULL)																		/* On verifie le fichier est ouvert */
  {
    printf("Fichier inconnu\n");
    exit(-1);
  }
  else
  {
    printf(" \"%s\" ouvert\n", nomFichier);

    while(!feof(f))
    {
      n = fscanf(f, "%s",copy);										/* On lit la ligne */

      if(n != -1)                            		  /* On verifie qu'on a lu un message */
      {
        mot         = strtok(copy, ";");        	/* On copie le mot */
        traduction  = strtok(NULL, "\0"); 				
        insererCellule(table, mot, traduction);   /* On tente l'insertion */
      }
    }

    fclose(f);																	  /* On ferme le fichier */
  }
  return table;
}






/*******************************************************************************
 * Prend un mot en entree et recherche sa traduction dans la table en argument.
 * Si le mot n'est pas present dans la table, on retourne le mot en argument
 * tel quel en ajoutant un message d'erreur.
 *
 * @entree    	 mot:  String contenant le mot qu'on cherche a traduire
 * @entree 		 table:  Pointeur de la table majeure contenant le dictionnaire
 * 
 * @local   parcours:  Pointeur pour chercher le 'mot' dans la table majeure
 * 
 * @return  	retour:  String contenant la traduction du mot
 *                     Ou le mot tel quel s'il n'existe pas dans le dictionnaire
 ******************************************************************************/
char * traductionMot(char * mot, majeur_t * table)
{
  cellule_t	*   parcours  = NULL;
  char      *     retour  = mot;          /* Le mot en argument est la valeur par defaut */
	
	                                          
  parcours = rechercherMot(table, mot);   /* On recherche l'adresse du mot dans la table majeure */

  if (parcours)                           /* Si le parcours a abouti */
  {
    printf("%s = %s\n", parcours->mot, parcours->traduction ); /* Affichage */
    retour = parcours->traduction;        /* On recupere sa traduction */
  }
  else
  {
    printf("Le mot  \"%s\" n'a pas de traduction\n", mot);
  }

  return retour;
}





/*******************************************************************************
 * Traduit une 'phrase' toute entiere en traduisant mot a mot a partir du
 * dictionnaire present dans la 'table' passee en argument.
 * La traduction complete est retournee dans la String 'retour'
 *
 * @entree    phrase:  String contenant la phrase entiere a traduire
 * @entree 		 table:  Pointeur de la table majeure
 * @entree 		retour:  Adresse ou ecrire la traduction de la phrase entiere
 * 
 * @local  		 copie:  Copie de la phrase d'origine pour ne pas modifier 'phrase'
 * @local  			 tok:  Adresse du mot courant a traduire
 * 
 * @return 		retour:  String contenant la traduction complete de la phrase
 *                     Les mots non traduits restent dans la langue d'origine
 ******************************************************************************/
void traductionPhrase(char * phrase, majeur_t * table, char * retour)
{
  char *               tok = NULL;
  char   copie[TAILLE_MAX] = {0};

  strcpy(copie, phrase);

  tok = strtok(copie, " ");

  while( tok )                                  /* Tant qu'on lit un nouveau mot */
  {    
    strcat(retour, traductionMot(tok, table));  /* On concatene la traduction */
    strcat(retour, " ");

    tok = strtok(NULL, " ");                    /* On passe au mot suivant */
  }
}



/*******************************************************************************
 * Fonction qui libere les allocations memoire des sous-chaines puis la table
 * majeure.
 *
 * @entree 		 table:  Pointeur de la table majeure
 * 
 * @local   parcours:  Pointeur de parcours des sous-chaines
 * @local  			 lib:  Pointeur temporaire pour les liberations
 * @local          i:  Indice de parcours
 ******************************************************************************/
void libererTable( majeur_t ** table)
{
  cellule_t	*	parcours = NULL;
  cellule_t	*	     lib = NULL;
  int                i = 0;

  /* On parcourt tous les etages de la table majeure */
  if(*table == NULL)
  {
    puts("libererTable(): Table non allouee");
  }
  else
  {
    for(i = 0; i < HASH_MAX; i++)
    {
      parcours  = (*table)->chaine[i];
      
      while (parcours) 					 /* Liberation de la sous-chaine */
      {                          /* (On aurait pu utiliser une liberation de liste chainee) */
        lib       = parcours;
        parcours  = parcours->suivant;
        
        free(lib->mot);
        free(lib->traduction);
        free(lib);
      }
    }

    free(*table);									/* Liberation la table majeure */
    * table = NULL;

    printf("Table liberee\n");
  }
  puts("Fin de la liberation\n");
}




/******************************************************************************/
/************************************** BONUS *********************************/
/******************************************************************************/

/*******************************************************************************
 * Calcule et retourne la longueur moyenne des sous-chaines de la table
 *
 * @entree 		 table:  Pointeur de la table majeur
 * 
 * @local      somme:  Somme le nombre d'elements presents dans chaque sous-chaine
 * @local          i:  Indice de parcours
 * 
 * @return          :  Double, moyenne du nombre d'elment par sous-chaine
 ******************************************************************************/
float longueurMoyenneSousTables(majeur_t * table)
{
  int somme  = 0;
  int     i  = 0;

	/* On parcourt la deuxième colonne de la table majeur 
     Cette colonne correspond au nombre d'elements present
     dans la sous-chaine associee a ce meme indice 'i' */
  for(i = 0; i < HASH_MAX; i++)
  {
      somme += table->NBelement[i];				
  }
  printf("La table possede %d elements\n", somme);
  
  return (float) somme / HASH_MAX;
}
