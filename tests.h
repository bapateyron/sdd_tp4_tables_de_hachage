/*******************************************************************************
*                               TESTS.H
*
* Fichier en-tete qui declare les prototypes de toutes les fonctions de test
* des modules qui seront appelees dans le main
*
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "hachage.h"

void test_liberation();
void test_recherche();
void test_insertion();
void test_traduction();
void test_longueurMoyenneSousChaines();





