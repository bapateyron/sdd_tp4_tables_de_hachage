/*******************************************************************************
 *                                   MAIN.C
 *
 * Point d'entree du programme. Se contente d'executer les fonctions de test
 * En utilisant pour la plupart d'entre elles le fichier 'anglais.txt'
 *
 * Il est possible de prendre un fichier parametre en decommentant la partie
 * intitulee "test manuel" ainsi que les arguments de la fonction main().
 *
 ******************************************************************************/

#include "tests.h"

int main(/* int argc, char ** argv */)
{
  
  /*      / ! \        */
  /* Decommenter les tests manuels et les arguments de main() pour utiliser vos propres fichiers */
  /*      / ! \        */

  /********************* Test manuel ***************************************/

      /* majeur_t *      table = NULL; */
      /* char 	 nomFichier[50]	= "anglais.txt"; */        /* Nom du fichier a ouvrir (anglais.txt par defaut) */
      /* char     retour[1000] = {0}; */                  /* Espace de reception de la traduction de phrase */
      
      /* if(argc == 2) strcpy(nomFichier, argv[1]); */    /* Ouvre le fichier en parametre s'il existe */



      /* table = importerListe(nomFichier); */                                  /* Ouverture du fichier  */

      /* printf("Longueur moyenne: %f\n", longueurMoyenneSousTables(table)); */ /* Test longueur moyenne */

      /* insererCellule(table, "foot", "pied"); */                              /* Test traduction insertion */

      /* traductionPhrase("hello bye sorry i am cheese end", table, retour); */ /* Test traduction phrase */

      /* printf("Longueur moyenne: %f\n", longueurMoyenneSousTables(table)); */ /* Test longueur moyenne */

      /* libererTable(&table); */

  /********************* Test manuel    [ FIN ]  ***************************/




	/********************* Tests automatises *********************************/

	puts("\n-------- Debut du programme de test -------------\n\n");

  test_liberation();
	test_recherche();
	test_insertion();
	test_traduction();
	test_longueurMoyenneSousChaines();



	/********************* Tests automatises      [ FIN ] ********************/

	puts("-------- Fin du programme ------------------------\n");


	return 0;
}
